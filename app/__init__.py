from flask import Flask
from flask_mail import Mail, Message

from app.config import Config

app = Flask(__name__)
app.config.update(
    DEBUG=Config.DEBUG,
    MAIL_SERVER=Config.MAIL_SERVER,
    MAIL_PORT=Config.MAIL_PORT,
    MAIL_USE_SSL=Config.MAIL_USE_SSL,
    MAIL_USERNAME=Config.MAIL_USERNAME,
    MAIL_DEFAULT_SENDER=Config.MAIL_USERNAME,
    MAIL_PASSWORD=Config.MAIL_PASSWORD
)

mail = Mail(app)

@app.route('/sendmail')
def send_mail():
    try:
        msg = Message(
            'Flask Email',
            recipients=['rusditoriq101@gmail.com',]
            )
        msg.body = 'This is test email by flask.'
        mail.send(msg)
        return 'Mail Sent'
    except Exception as e:
        return str(e)